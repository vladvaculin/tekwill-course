/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.exception;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class MyException {
    public static void main(String[] args) throws IOException {
       FileReader fr = null;
       BufferedReader bs = null;
       String line = null;

       File file = new File("C:\\Users\\user\\Desktop\\");

       System.out.println("----before try");
       try {
           fr = new FileReader("C:\\Users\\user\\Desktop\\debug_con.txt");
       } catch (FileNotFoundException e) {
           System.out.println("----in catch");
           System.out.println("File not Found ! Blin !");

           System.out.println("Others files in C:\\Users\\user\\Desktop: ");
           for (String s : file.list()) {
               System.out.println("\t " + s);
           }

           // try to repair the problem
//           fr = new FileReader("C:\\Users\\user\\Desktop\\debug_console.txt");
//            return;
       }
       System.out.println("----after catch");

       bs = new BufferedReader(fr);
       if ((line = bs.readLine()) != null) {
           System.out.println("our Line: \n\n" + line);
           line = bs.readLine();
       }
   }
}
