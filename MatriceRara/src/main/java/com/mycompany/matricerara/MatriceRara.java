/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.matricerara;

/**
 *
 * @author user
 */
public class MatriceRara {
    
    private final double values[];
    private final int column[];
    private final int rowPtr[];
    private int size;
    private int rows;
    
    
    
    public MatriceRara(double vector[]){
        this.rows = 0;
        this.size = 0;
        
        int len = calcInitSize(vector);
        
        values = new double[len];
        column = new int[len];
        rowPtr = new int[1];
        initValueArray(vector);
        
        
        
        
    }
    
    public MatriceRara(double matrix[][]){
        this.rows = 0;
        this.size = 0;
        int len = calcInitSize(matrix);
        
        values = new double[len];
        column = new int[len];
        rowPtr = new int[matrix.length];
        initValueArray(matrix);
        
        
    }
    
    
    private int calcInitSize(double vector[]){
        int n = vector.length;
        int j = 0;
        
        for (int i = 0; i < n; ++i){
            if (vector[i] != 0){
                j++;
            }
        }
        return j;
    }
    
    private int calcInitSize(double matrix[][]){
        int n = matrix.length;
        int j = 0;
        for (int i = 0; i < n; ++i){
            j += calcInitSize(matrix[i]);
        }
        return j;
    }
    
    
    private void initValueArray(double vector[]){
        int n = vector.length;
        boolean isFirst = true;
        
        for (int i = 0; i < n; ++i){
            if (vector[i] != 0){
                values[size] = vector[i];
                column[size] = i;
                if (isFirst){
                    rowPtr[rows] = size;
                    isFirst = false;
                    rows++;
                }
                size++;
            }
        }
        if (isFirst)
            rowPtr[rows++] = -1;
    }
    
    private void initValueArray(double matrix[][]){
        int n = matrix.length;
        
        for(int i = 0; i < n; ++i){
            initValueArray(matrix[i]);
        }

    }
    
    
    public static double[][] generate(int nrOfValues, int interval){
    
        return new double[1][1];
    }

    @Override
    public String toString(){
        return "new Matrice";
    }
    
}
