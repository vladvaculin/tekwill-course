/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myarraylist;

/**
 *
 * @author user
 */
public class MyArrayList {
    private final int[] vector = new int[12];
    private int index = 0;
    
    public void add(int value){
        if (index == 12){
            System.out.println("You can't add any items more, becouse vector is fuill");
            System.out.println("Try to remove items");
        } else {
            vector[index] = value;
            index++;
        }
    }
    
    public int get(int index){
        if (index >= 0 && index <= 11)
            return vector[index];
        else {
            System.out.println("Error!!! Index out of range");
            return -1;
        }
    }
    
    
    public boolean contains(int value){
        for (int i : vector) {
            if (i == value)
                return true;
        }
        return false;
    }
    
    public void remove(int index){
        if (index >= 0 && index <= 11){
             if (index == (this.index - 1)) {
            vector[index] = 0;
        } else {
            for (int i = index; i < this.index; ++i) {
                vector[i] = vector[i + 1];
            }
        }
        this.index--;
        }
        else {
            System.out.println("Error!!! Index out of range");
        }
    }
    
    public int size(){
        return index;
    }
    
}
