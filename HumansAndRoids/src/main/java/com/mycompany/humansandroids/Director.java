/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.humansandroids;

/**
 *
 * @author user
 */
public class Director extends Human{
    
    public Director(String name, int age){
        this.setName(name);
        this.setAge(age);
    }


    @Override
    void speak(String q) {
        System.out.println("DIRECTOR SAYS : \"" + q + "!\"");
        
    }
    
}
