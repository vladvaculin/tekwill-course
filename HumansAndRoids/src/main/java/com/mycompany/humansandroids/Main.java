/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.humansandroids;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class Main {
    public static void main(String[] args) {
        Human h = new Human() {
            @Override
            void speak(String q) {
                System.out.println("yo : " + q);
            }
        };
        Director d = new Director("john", 49);
        T1000 a;
        try {
            a = new T1000("XXX");
        } catch (MyException ex) {
            ex.printMyEx();
            return;
        }
        
        try{
            d = (Director) h;
        } catch(ClassCastException e){
            System.out.println("nonno");
            return;
        }
        d.speak("Who are YOU?");
        a.sayMyName();
    }
}
