/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.game;



/**
 *
 * @author user
 */
public class Game {
    private final Team homeTeam;
    private final Team guestTeam;
    private final String location;
    private final Goal[] goals;
    private int nrOfGoals = 0;
    
    private boolean gameStarted;
    private Team winner;
    private Team looser;
    private Goal[] homeTeamGoals;
    private int nrOfHomeTeamGoals;
    private Goal[] guestTeamGoals;
    private int nrOfGuestTeamGoals;
    
    public Game(Team homeTeam, Team guestTeam, String location){
        this.homeTeam = homeTeam;
        this.guestTeam = guestTeam;
        this.location = location;
        this.goals = new Goal[5];
    }
    
    public void start(){
        gameStarted = true;
        System.out.println("Welcome at " + location);
        System.out.println("Teams are getting ready for this exciting match!");
        System.out.println("Tonight play " + homeTeam.name + " and " + guestTeam.name);
        homeTeam.printPlayers();
        guestTeam.printPlayers();
        
    }
    
    public int getNrGoals(){
        return nrOfGoals;
    }
    public int getNrGoals(Team team){
        int i = 0;
        if (team == homeTeam) {
           for (int j = 0; j < nrOfGoals; j++){
               if (goals[j].getScoersTeamsName().equals(homeTeam.name)){
                   i++;
               }
           }
           return i;
        } else if (team == guestTeam) {
            for (int j = 0; j < nrOfGoals; j++){
               if (goals[j].getScoersTeamsName().equals(guestTeam.name)){
                   i++;
               }
           }
            return i;
        } else {
            System.err.println("Sorry, this team is not playing in this game!");
            return -1;
        }
    }
    
    private void end(){
        gameStarted = false;
        nrOfGuestTeamGoals = getNrGoals(guestTeam);
        nrOfHomeTeamGoals = getNrGoals(homeTeam);
        
        homeTeamGoals = new Goal[nrOfHomeTeamGoals];
        guestTeamGoals = new Goal[nrOfGuestTeamGoals];
        
        for (Goal goal : goals){
            int i = 0, j = 0;
               if (goal.getScoersTeamsName().equals(guestTeam.name)){
                   guestTeamGoals[i] = goal; i++;
               } else {
                   homeTeamGoals[j] = goal; j++;
               }
           }
        
        if (nrOfGuestTeamGoals > nrOfHomeTeamGoals){
            winner = guestTeam;
            looser = homeTeam;
        } else {
            winner = homeTeam;
            looser = guestTeam;
        }
        System.out.println(nrOfHomeTeamGoals + " - " + nrOfGuestTeamGoals + ". Won " + winner.name);
        
    }
    
    public void score(Goal goal){
        if (this.gameStarted){
            goals[nrOfGoals] = goal;
            nrOfGoals++;
            if (nrOfGoals == 5){
                System.out.println("And this was the last GOAL from " + goal.getScorersName());
                this.end();
            } else {
                double time = goal.getTime();
                System.out.println("Nice kick from " + goal.getScorersName() + " scoring 1 goal for " + goal.getScoersTeamsName() + " at min " + time);       
                System.out.println("Now score is : " + getNrGoals(homeTeam) + " - " + getNrGoals(guestTeam));
            }
        } else {
            System.out.println("Sorry, you can't score if you are not playing");
        }
    }
    
}
