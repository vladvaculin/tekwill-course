/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.game;

/**
 *
 * @author user
 */
public class Player {
    
    public final String name;
    
   public Player(String name){
       this.name = name;
   }
   
   public String findTeam(){
       for (Team temp : League.teams) {
           if (temp.players.contains(this)){
               return temp.name;
           }
       }
       return "no one";
       
   }
   
    @Override
   public String toString(){
       String team = findTeam();
       return "Player : " + name + ", playing for " + team;
   }
    
}
