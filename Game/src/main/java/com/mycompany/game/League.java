/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.game;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author user
 */
public class League {
    
    public static List<Team> teams = new ArrayList<Team>();
    
    
    public static void main(String[] args) {
        
        //Team A
        Player antonio = new Player("Antonio");
        Player fernando = new Player("Fernando Torres");
        Player messi = new Player("Messi");
        Player xavi = new Player("Xavi");
        
        List<Player> teamAPlayers = new ArrayList<>();
        teamAPlayers.add(antonio);
        teamAPlayers.add(fernando);
        teamAPlayers.add(messi);
        teamAPlayers.add(xavi);
        
        Team teamA = new Team("Chisinau Barca", teamAPlayers);
        teams.add(teamA);
        
        //Team B
        Player cristiano = new Player("Cristiano Ronaldo 7");
        Player ronadlinio = new Player("Ronaldinho");
        Player toni = new Player("Toni");
        Player neymar = new Player("Neymar");
        
        List<Player> teamBPlayers = new ArrayList<>();
        teamBPlayers.add(cristiano);
        teamBPlayers.add(ronadlinio);
        teamBPlayers.add(toni);
        teamBPlayers.add(neymar);
        
        Team teamB = new Team("Real Balti", teamBPlayers);
        teams.add(teamB);
        
        Game game = new Game(teamA, teamB, "Chisinau Zimbru");
        
        Goal firstGoal = new Goal(neymar, 0);
        Goal secondGoal = new Goal(ronadlinio, 10);
        Goal thirdGoal = new Goal(toni, 13);
        Goal fourthtGoal = new Goal(messi, 79);
        Goal fithGoal = new Goal(messi, 80);
        
        
        game.start();
        game.score(firstGoal);
        game.score(secondGoal);
        game.score(thirdGoal);
        game.score(fourthtGoal);
        game.score(fithGoal);
        
        
        System.out.println(antonio);
    }
}
