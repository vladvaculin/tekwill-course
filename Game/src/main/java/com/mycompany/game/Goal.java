/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.game;

/**
 *
 * @author user
 */
public class Goal {
    private final Player scorer;
    private final double time;
    
    
    public Goal(Player scorer, double time){
        this.scorer = scorer;
        this.time = time;
    }
    
    public String getScoersTeamsName(){
        return scorer.findTeam();
    }
    
    public String getScorersName(){
        return scorer.name;
    }
    
    public double getTime(){
        return time;
    }
}
