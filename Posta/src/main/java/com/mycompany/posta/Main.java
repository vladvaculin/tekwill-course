/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.posta;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author user
 */
public class Main {

   private static List<Report> report;
   private static String trackingNumber;


   public static void main(String[] args) throws IOException {

//       final String EXAMPLE_TEST = "This is my small example "
//            + "string which I'm going to " + "use for pattern matching.";
//       System.out.println(EXAMPLE_TEST.matches("\\w.*"));
//        String[] splitString = (EXAMPLE_TEST.split("\\s+"));
//        System.out.println(splitString.length);// should be 14
//        for (String string : splitString) {
//            System.out.println(string);
//        }
//        // replace all whitespace with tabs
//        System.out.println(EXAMPLE_TEST.replaceAll("\\s+", "\t"));
//       
       
       
       report = new ArrayList<>();

       Scanner scanner = new Scanner(System.in);
       System.out.println("Insert tracking number: ");
       trackingNumber = scanner.nextLine();

       String defaultURL = "http://www.posta.md/en/tracking?id=RB295349616SG";/*RS916319658NL*/
       int indexTrackingNumber = defaultURL.indexOf("=");

       String defaultTrackingNumber = defaultURL.substring(indexTrackingNumber + 1);

       String url = defaultURL.replace(defaultTrackingNumber, trackingNumber);
       boolean isError = false;
       URL postaMoldovei = new URL(url);
       URLConnection connection = postaMoldovei.openConnection();
       try (BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
           String inputLine;
           Report r ;
           while ((inputLine = in.readLine()) != null) {
//               System.out.println(inputLine);
               //TODO DO the job !!!
               if(inputLine.contains("row clearfix")){
                   r = new Report();
                   
                   
                   r.setDate(parseLine(in.readLine()));
                   r.setCountry(parseLine(in.readLine()));
                   r.setLocation(parseLine(in.readLine()));
                   r.setEventType(parseLine(in.readLine()));
                   r.setExtraInfo(parseLine(in.readLine()));
                   report.add(r);
               }
               if(inputLine.contains("error-message")){
                   isError = true;
                   break;
               }
               
           }
       }
       if (isError){
           System.out.println("The number is not valid!!!");
       } else
            System.out.println(report);
   }

   private static String parseLine(String inputLine) {
//       int start = inputLine.indexOf("\">");
//       int end = inputLine.indexOf("</");
//       String r = inputLine.substring(start + 2, end);
//        
//       return r;

    return StringUtils.substringBetween(inputLine, ">", "<");
   }


}
