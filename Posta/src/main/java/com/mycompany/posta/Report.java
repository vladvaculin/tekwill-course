/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.posta;

/**
 *
 * @author user
 */
class Report {
    private String date;
    private String country;
    private String location;
    private String eventType;
    private String extraInfo;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getExtraInfo() {
        return extraInfo;
    }

    public void setExtraInfo(String extraInfo) {
        this.extraInfo = extraInfo;
    }

    @Override
    public String toString() {
        return "\nReport{" + 
                "\ndate = " + date + 
                "\ncountry = " + country + 
                "\nlocation = " + location + 
                "\neventType = " + eventType + 
                "\nextraInfo = " + extraInfo + "\n}";
    }
    
    
    
}
