/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.methodhiding;

/**
 *
 * @author user
 */
public class ClassB extends ClassA {

    static int X = 20;

    static void run() {
        System.out.println("ClassB run");
    }

    int y = 40;

    int z = 1;
    public static void main(String[] args) {
        ClassA a = new ClassA();
        ClassA ab = new ClassB();
        ClassB b = new ClassB();
        
        System.out.println("--------------------------------");
        System.out.println("a.X = " + a.X);
        System.out.println("ab.X = " + ab.X);
        System.out.println("b.X = " + b.X);
        System.out.println("--------------------------------");
        System.out.println("a.Y = " + a.y);
        System.out.println("ab.Y = " + ((ClassB)ab).y);
        System.out.println("b.Y = " + b.y);
        System.out.println("--------------------------------");
        a.run();
        ab.run();
        b.run();
    }
}

abstract class Vehicle{
    int a;
    
}