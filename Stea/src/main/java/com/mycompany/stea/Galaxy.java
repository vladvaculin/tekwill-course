/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.stea;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author user
 */
public class Galaxy {
    public static void main(String[] args) {
        Stea stea;
        
        List<String> planetNames = new ArrayList<>();
        List<Float> planetWeights = new ArrayList<>();
        
        planetNames.add("Pamant");
        planetNames.add("Marte");
        
        planetWeights.add(544f);
        planetWeights.add(2335f);
        stea = new Stea("Soare", 654987, planetNames, planetWeights);
        
//        System.out.println(stea);
        Stea s2 = new Stea();
        System.out.println(stea);
        System.out.println(s2);
        System.out.println("\n\n===============Black Hole===========\n\n");
        Stea.blackHole(stea, s2);
        
        System.out.println(stea);
        System.out.println(s2);
        
    }
}
