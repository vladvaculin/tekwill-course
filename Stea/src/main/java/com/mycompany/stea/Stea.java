/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.stea;

import java.util.ArrayList;
import java.util.List;
//import java.lang.
import java.util.Scanner;

/**
 *
 * @author user
 */
public class Stea {
    
    private final String name;
    public double weight;
    private int numberOfPlanets;
    
    public List<Float> planetWeights;
    private List<String> planetNames;
    private static int nrOfObjects;

    public Stea() {
        Scanner scanner = new Scanner(System.in);
        
        
        System.out.print("Numele : ");          
        this.name = scanner.nextLine();
        
        System.out.print("Greutatea : ");       
        this.weight = scanner.nextDouble();
        
        System.out.print("Numarul de planete a stelei : ");   
        this.numberOfPlanets = scanner.nextInt();
        this.planetWeights = new ArrayList<>();
        this.planetNames = new ArrayList<>();
        
        for (int i = 0; i < this.numberOfPlanets; ++i){
            
            System.out.println("Planeta " + (i+1) + " : ");
            
            System.out.print("  Numele : ");
            scanner.nextLine();
            planetNames.add(scanner.nextLine());
            System.out.print("    Greutatea : ");
            planetWeights.add(scanner.nextFloat());
            
        }
        nrOfObjects++;
    }
    
    public Stea(String name, double weight, List<String> planetNames, List<Float> planetWeights ) {
        this.name = name;
        this.weight = weight;
        
        if (planetNames.size() != planetWeights.size() ){
            throw new IllegalArgumentException("Sizes do not match!");
        } else {
            
            this.planetNames = new ArrayList<>(planetNames);
            this.planetWeights = new ArrayList<>(planetWeights);
            this.numberOfPlanets = planetWeights.size();
        }
        nrOfObjects++;
    }

    @Override
    public String toString() {
        return "Stea{\n" + 
                "Name = " + name + 
                "\nWeight = " + weight + 
                "\nNumber Of Planets = " + numberOfPlanets + 
                "\nPlanets' Weight = " + planetWeights + 
                "\nPlanets' Name = " + planetNames + '}';
    }

    public float getMassOfAllPlanets(){
        float s = 0.0f;
        for (Float planetWeight : planetWeights) {
            s += planetWeight;
        }
        
        return s;
    }
    
    public boolean equals(Stea stea){
        return (this.getMassOfAllPlanets() == stea.getMassOfAllPlanets());
    }

    
    public static void blackHole(Stea s1, Stea s2){
        Stea greater = (s1.weight > s2.weight) ? s1 : s2;
        Stea smaller = (s1.weight > s2.weight) ? s2 : s1;
        
        greater.weight += smaller.weight;
        smaller.weight = 0;
        
        greater.planetWeights.forEach((Float w) -> {
            greater.weight += w;
            w = 0.0f;
        });
        smaller.planetWeights.forEach((w) -> {
            greater.weight += w;
            w = 0.0f;
        });
    }
    public int getNumberOfPlanets() {
        return numberOfPlanets;
    }

    public void setNumberOfPlanets(int numberOfPlanets) {
        this.numberOfPlanets = numberOfPlanets;
    }

    public List<Float> getPlanetWeights() {
        return planetWeights;
    }

    public void setPlanetWeights(List<Float> planetWeights) {
        this.planetWeights = planetWeights;
    }

    public List<String> getPlanetNames() {
        return planetNames;
    }

    public void setPlanetNames(List<String> planetNames) {
        this.planetNames = planetNames;
    }

    public static int getNrOfObjects() {
        return nrOfObjects;
    }

    public static void setNrOfObjects(int nrOfObjects) {
        Stea.nrOfObjects = nrOfObjects;
    }
    
    
    
    
}
